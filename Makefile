slugify_jpg:
	find . -type f -name '*.jpeg' -print0 | xargs -0 rename 's/\.jpeg/\.jpg/'
	rename 's/\.(?!jpg$$)/ /g' *.jpg
	/home/david8z/Functions/slugify -v *

optimize_png:
	optimize-images -mh 615 -q 100 .
	optimize-images -q 100 -ca -fd .
	rename 's/\.(?!jpg$$)/ /g' *.jpg
	/home/david8z/web_ecommerce/fotos-zapatos/slugify -v *
