#!/bin/sh

cd backend/famaBackend/

# collect static files
python manage.py collectstatic --noinput


python manage.py makemigrations --merge

python manage.py migrate --noinput

exec "$@"