from django.contrib import admin
from .models import Entrada
from django.utils.html import mark_safe
from django.conf import settings
# Register your models here.

class EntradaModelAdmin(admin.ModelAdmin):
    list_display = ['slug',  'imagen']

    def imagen(self, obj):
        if settings.DEBUG:
            return mark_safe(
                    '<img src="http://127.0.0.1:8080/media/%s" width="200" height="200">' % obj.imagen_thumbnail
                )
        else:
            return mark_safe(
                    '<img src="%s/media/%s" width="200" height="200">' % (settings.URL_PROD, obj.imagen_thumbnail)
                )

    fieldsets = (
        ('METADATOS', {
            'fields': (('meta_titulo', 'meta_descripcion'),),
            'classes': ('grp-collapse grp-closed',),
        }),
        ('IMÁGENES', {
            'fields': (('imagen_thumbnail', 'alt_imagen_thumbnail'), ('imagen_principal', 'alt_imagen_principal')),
            'classes': ('grp-collapse grp-open',),
        }),
        ('CONTENIDOS', {
            'fields': ('titulo', 'slug', 'contenido'),
            'classes': ('grp-collapse grp-open',),
        })
    )
    class Media:
        css = {
            'all': ('admin/css/custom-row.css',)
        }

admin.site.register(Entrada, EntradaModelAdmin)