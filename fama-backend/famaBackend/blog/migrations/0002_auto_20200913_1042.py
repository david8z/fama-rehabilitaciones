# Generated by Django 3.1.1 on 2020-09-13 08:42

import ckeditor_uploader.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entrada',
            name='contenido',
            field=ckeditor_uploader.fields.RichTextUploadingField(verbose_name='Contenido de la Entrada*'),
        ),
    ]
