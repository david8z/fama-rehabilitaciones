from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.db.models.signals import pre_save
from django.utils.text import slugify

class Entrada(models.Model):
    """
    """
    slug = models.SlugField(primary_key=True, max_length=100, blank=True, verbose_name="Slug", help_text="Será el nombre que tendrá la url, por defecto es el nombre del titulo en formato slug.")
    titulo = models.CharField(max_length=100, verbose_name="Título*", unique=True)
    
    imagen_thumbnail = models.ImageField(upload_to='blog', help_text="Será la primera imágen que aparecerá en los recuadros de selección de la entrada formato recomendado 1:1.", verbose_name="Imagen Thumbnail*", null=True)
    alt_imagen_thumbnail = models.TextField(max_length=300, null=True, blank=True, verbose_name='Alt')
    
    imagen_principal = models.ImageField(upload_to='blog', help_text="Será la primera imágen que aparecerá en la entrada del blog.", verbose_name="Imagen Principal*", null=True)
    alt_imagen_principal = models.TextField(max_length=300, null=True, blank=True, verbose_name='Alt')

    contenido = RichTextUploadingField(verbose_name="Contenido de la Entrada*", config_name='awesome_ckeditor')

    meta_titulo = models.CharField(max_length=60, null=True, blank=True, help_text="Máximo 60 carácteres", verbose_name="Meta Título")
    meta_descripcion = models.TextField(max_length=120, null=True, blank=True, help_text="Máximo 120 carácteres", verbose_name="Meta Descripción")

def blog_pre_save_receiver(sender, instance, *args, **kwargs):
    """
    """
    instance.slug = slugify(instance.titulo)

pre_save.connect(blog_pre_save_receiver, sender=Entrada)
