import django.core.checks

from rest_framework import serializers
from .models import Entrada

@django.core.checks.register('rest_framework.serializers')
class EntradaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Entrada
        fields = ['slug', 'titulo', 'imagen_principal', 'alt_imagen_principal', 'contenido', 'meta_titulo', 'meta_descripcion']

@django.core.checks.register('rest_framework.serializers')
class EntradaThumbnailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Entrada
        fields = ['slug', 'titulo', 'imagen_thumbnail', 'alt_imagen_thumbnail']
