from rest_framework import viewsets
from .models import Entrada
from .serializers import EntradaSerializer, EntradaThumbnailSerializer
from rest_framework.response import Response

class EntradaViewSet(viewsets.ModelViewSet):
    """
    """
    serializer_class = EntradaSerializer
    lookup_field = 'slug'
    http_method_names = ['get']

    def get_queryset(self):
        return Entrada.objects.all()
  
    def list(self, request):
        queryset = self.get_queryset()
        serializer = EntradaThumbnailSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, slug):
        queryset = Entrada.objects.filter(slug=slug).first()
        serializer = EntradaSerializer(queryset, many=False)
        return Response(serializer.data)