from django.contrib import admin
from .models import Configuracion

class ConfiguracionModelAdmin(admin.ModelAdmin):
    list_display = ['configuracion',]

    def configuracion(self, obj):
        return 'Configuración FAMA'

    fieldsets = (
        ('CONTACTO', {
            'fields': ('nombre_empresa','correo', ('telefono_fijo', 'telefono_movil'),('direccion_linea_1','direccion_linea_2')),
            'classes': ('grp-collapse grp-open',),
        }),
        ('REDES SOCIALES', {
            'fields': (('enlace_fb', 'enlace_ig'),),
            'classes': ('grp-collapse grp-open',),
        }),
        ('ANALÍTICA', {
            'fields': (('codigo_pixel_fb', 'codigo_google_analytics'),),
            'classes': ('grp-collapse grp-open',),
        }),
        ('IMÁGENES', {
            'fields': (('logo_negro', 'logo_blanco'), 'favicon'),
            'classes': ('grp-collapse grp-open',),
        }),
    )

    def has_add_permission(self, request, obj=None):
        return request.user.username == 'admin'
    
    def has_delete_permission(self, request, obj=None):
        return request.user.username == 'admin'
    
    class Media:
        css = {
            'all': ('admin/css/custom-row.css',)
        }

admin.site.register(Configuracion, ConfiguracionModelAdmin)
