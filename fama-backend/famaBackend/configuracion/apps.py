from django.apps import AppConfig


class ConfiguracionConfig(AppConfig):
    name = 'configuracion'
    verbose_name = 'Configuración FAMA'
