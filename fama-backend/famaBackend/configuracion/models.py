from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

class Configuracion(models.Model):
    # CONTACTO
    nombre_empresa = models.CharField(max_length=200, default="Fama Rehabilitaciones, S.L.U", verbose_name="Nombre Empresa*")
    correo = models.EmailField(verbose_name='Email*', help_text="Email que aparecerá en la web como email de contacto.")
    telefono_fijo = models.PositiveIntegerField(validators=[MinValueValidator(600_000_000), MaxValueValidator(999_999_999)], verbose_name="Teléfono Fijo*")
    telefono_movil = models.PositiveIntegerField(validators=[MinValueValidator(600_000_000), MaxValueValidator(999_999_999)], verbose_name="Teléfono Móvil*")
    direccion_linea_1 = models.CharField(max_length=200, verbose_name="Dirección Línea 1*")
    direccion_linea_2 = models.CharField(max_length=200, verbose_name="Dirección Línea 2*")

    # REDES SOCIALES
    enlace_fb = models.CharField(max_length=300, verbose_name="Enlace Facebook", null=True, blank=True)
    enlace_ig = models.CharField(max_length=300, verbose_name="Enlace Instagram", null=True, blank=True)

    # CÓDIGOS
    codigo_pixel_fb = models.CharField(max_length=15, verbose_name="Código Píxel de Facebook", null=True, blank=True)
    codigo_google_analytics = models.CharField(max_length=16, verbose_name="Código Google Analytics", null=True, blank=True)

    # IMÁGENES
    logo_negro = models.ImageField(upload_to="configuracion", verbose_name="Logo Oscuro*", help_text="Para evitar que aparezca de forma no deseada, cropear imágen del logo eliminando márgenes internos de imágen")
    logo_blanco = models.ImageField(upload_to="configuracion", verbose_name="Logo Claro*", help_text="Para evitar que aparezca de forma no deseada, cropear imágen del logo eliminando márgenes internos de imágen")
    favicon = models.ImageField(upload_to="configuracion", verbose_name="Favicon", help_text="Formato .ico, tamaño 196x196", null=True, blank=True)

    def __str__(self):
        return "Configuración FAMA"

    class Meta:
        verbose_name = "Configuración"
        verbose_name_plural = "Configuración"


