import django.core.checks


from rest_framework import serializers
from .models import Configuracion


########################
##CONFIGURACIÓN SERVER##
########################
@django.core.checks.register('rest_framework.serializers')
class ConfiguracionServerSerializer(serializers.ModelSerializer):
    """DOCSTRING"""

    class Meta:
        model = Configuracion
        fields = ['codigo_pixel_fb', 'codigo_google_analytics', 'favicon']

########################
##CONFIGURACIÓN CLIENT##
########################
@django.core.checks.register('rest_framework.serializers')
class ConfiguracionClientSerializer(serializers.ModelSerializer):
    """DOCSTRING"""

    class Meta:
        model = Configuracion
        fields = ['nombre_empresa','correo', 'telefono_fijo', 'telefono_movil', 'direccion_linea_1', 'direccion_linea_2', 'enlace_fb', 'enlace_ig', 'logo_negro', 'logo_blanco']