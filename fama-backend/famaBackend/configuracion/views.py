from rest_framework.response import Response
from django.http import HttpResponse
from rest_framework.decorators import api_view
from .models import Configuracion
from .serializers import ConfiguracionClientSerializer, ConfiguracionServerSerializer

@api_view(['GET'])
def configuracion_server(request):
    """
    API endpoint permite obtener información configuración requerida en server.
    """
    try:
        queryset = Configuracion.objects.first()
        serializer = ConfiguracionServerSerializer(queryset, many=False)
        return Response(serializer.data)
    except:
        return HttpResponse(status=400)

@api_view(['GET'])
def configuracion_client(request):
    """
    API endpoint permite obtener información configuración requerida en cliente.
    """
    try:
        queryset = Configuracion.objects.first()
        serializer = ConfiguracionClientSerializer(queryset, many=False)
        return Response(serializer.data)
    except:
        return HttpResponse(status=400)
        
