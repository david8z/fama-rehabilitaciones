"""famaBackend URL Configuration
"""
from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
from django.urls import path, include

from configuracion.views import configuracion_client, configuracion_server
from streamblocks.views import page_view

from rest_framework import routers
from proyectos.views import ProyectoViewSet
from filtros.views import ServiciosViewSet, años_view, localidad_view, proyectista_view
from blog.views import EntradaViewSet

ROUTER = routers.DefaultRouter()
ROUTER.register(r'proyectos', ProyectoViewSet, basename="Proyectos")
ROUTER.register(r'servicios', ServiciosViewSet, basename="Servicios")
ROUTER.register(r'entradas', EntradaViewSet, basename="Entradas")

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(ROUTER.urls)),
    path('api/año/', años_view, name='años_view'),
    path('api/configuracion-client/', configuracion_client, name='configuracion_client'),
    path('api/configuracion-server/', configuracion_server, name='configuracion_server'),
    path('api/localidad/', localidad_view, name='localidad_view'),
    path('api/pagina-home/', page_view, name='page_view'),
    path('api/proyectista/', proyectista_view, name='proyectista_view'),
    path('grappelli/', include('grappelli.urls')),
    path('streamfield/', include('streamfield.urls')),
    path('ckeditor/', include('ckeditor_uploader.urls')),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
