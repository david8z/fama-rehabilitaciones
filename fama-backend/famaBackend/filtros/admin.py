from django.contrib import admin

from .models import Año, Proyectista, Localidad, Servicio
from django.conf import settings
from django.utils.html import mark_safe

class ServicioModelAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'imagen_display', 'position', )
    list_editable = ('position', )
    
    
    def imagen_display(self, obj):
        if settings.DEBUG:
            return mark_safe(
                    '<img src="http://127.0.0.1:8080/media/%s" width="200" height="200">' % obj.imagen
                )
        else:
            return mark_safe(
                    '<img src="%s/media/%s" width="200" height="200">' % (settings.URL_PROD, obj.imagen)
                )

    fieldsets = (
        ('METADATOS', {
            'fields': (('meta_titulo', 'meta_descripcion'),),
            'classes': ('grp-collapse grp-closed',),
        }),
        ('CONTENIDOS', {
            'fields': ('nombre', ('imagen', 'alt_imagen'), 'descripcion_corta', 'descripcion', 'mini_descripcion_home', 'texto_contacto')
        }),
    )
    sortable_field_name = "position"

    def has_delete_permission(self, request, obj=None):
        return False
    
    class Media:
        css = {
            'all': ('admin/css/full-row.css',)
        }
        js = (
            'admin/js/admin_list_reorder.js',
        )

class AñoModelAdmin(admin.ModelAdmin):
    list_display = ['año']
    fields = ('año', )
    def get_model_perms(self, request):
        if request.user.username == 'admin':
            return {'add': True, 'change':True, 'delete':True}
        return {}
    class Media:
        css = {
            'all': ('admin/css/custom.css',)
        }

class LocalidadModelAdmin(admin.ModelAdmin):
    list_display = ['nombre']
    fields = ('nombre', )
    def get_model_perms(self, request):
        if request.user.username == 'admin':
            return {'add': True, 'change':True, 'delete':True}
        return {}
    class Media:
        css = {
            'all': ('admin/css/custom.css',)
        }

class ProyectistaModelAdmin(admin.ModelAdmin):
    list_display = ['nombre']
    fields = ('especialidad', 'nombre', 'pagina_web', 'logo',)
    # def get_model_perms(self, request):
    #     return {}
    
    def has_delete_permission(self, request, obj=None):
        return request.user.username == 'admin'

    class Media:
        css = {
            'all': ('admin/css/custom.css',)
        }

admin.site.register(Servicio, ServicioModelAdmin)
admin.site.register(Año, AñoModelAdmin)
admin.site.register(Localidad, LocalidadModelAdmin)
admin.site.register(Proyectista, ProyectistaModelAdmin)
