# Generated by Django 3.1 on 2020-09-04 08:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('filtros', '0011_auto_20200806_2012'),
    ]

    operations = [
        migrations.AddField(
            model_name='servicio',
            name='mini_descripcion_home',
            field=models.CharField(blank=True, help_text='Texto que aparece con hover en lista de servicios. Máx 100 caracteres.', max_length=100, null=True, verbose_name='Descripcón Home Page'),
        ),
    ]
