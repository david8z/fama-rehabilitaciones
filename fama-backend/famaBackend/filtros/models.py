from django.db import models
from django.utils.text import slugify
from ckeditor.fields import RichTextField
from django.db.models.signals import pre_save


class Servicio(models.Model):
    SERVICIOS = (
        ('interiorismo', 'Interiorismo'),
        ('rehabilitacion', 'Rehabilitación'),
        ('obra-nueva', 'Obra Nueva')
    )
    nombre = models.CharField(primary_key=True, max_length=14, choices=SERVICIOS, verbose_name="Nombre*")
    descripcion = RichTextField(help_text="Descripción del servicio", verbose_name="Descripción", null=True, blank=True)
    descripcion_corta = models.CharField(help_text="Descripción corta del servicio que aparece justo debajo del nombre del servicio. Máx 100 caracteres.", max_length=100,verbose_name="Descripcón Corta", null=True, blank=True)
    mini_descripcion_home = models.CharField(max_length=100,verbose_name="Descripcón Home Page", null=True, blank=True, help_text="Texto que aparece con hover en lista de servicios. Máx 100 caracteres.")

    meta_titulo = models.CharField(max_length=100, null=True, blank=True)
    meta_descripcion = models.TextField(max_length=300, null=True, blank=True)

    imagen = models.ImageField(upload_to='servicios', help_text="Será la imágen que aparecerá en la cabecera del servicio.", verbose_name="Imagen*")
    alt_imagen = models.TextField(max_length=300, null=True, blank=True, verbose_name='Alt')

    position = models.IntegerField(verbose_name="Posición", null=True, blank=True, help_text="Dicatará el orden en el que aparecen los servicios en la lista de enlaces. Se genera de forma automática.")

    texto_contacto = models.CharField(max_length=100, null=True, blank=True, verbose_name="Texto de Contacto")

    def save(self, *args, **kwargs):
        model = self.__class__

        if self.position is None:
            # Append
            try:
                last = model.objects.order_by('-position')[0]
                self.position = last.position + 1
            except IndexError:
                # First row
                self.position = 0

        return super(Servicio, self).save(*args, **kwargs)

    def __str__(self):
        return 'Servicio: ' + self.get_nombre_display()


    class Meta:
        verbose_name_plural = "Servicios"
        ordering = ['position',]


class Año(models.Model):
    slug = models.SlugField(primary_key=True, max_length=4, blank=True)
    año = models.CharField(max_length=4, unique=True)

    def __str__(self):
        return 'Año: ' + self.año


class Proyectista(models.Model):
    slug = models.SlugField(primary_key=True, max_length=300, blank=True)
    nombre = models.CharField(max_length=300, unique=True, verbose_name="Nombre*")
    pagina_web = models.CharField(max_length=500, null=True, blank=True, verbose_name="Página Web")
    logo = models.ImageField(upload_to="logo_proyectista", null=True, blank=True, verbose_name="Logo")
    especialidad =  models.CharField(max_length=100, default="Proyectista", verbose_name="Especialidad*", help_text="Nombre de la especialidad del Proyectista.")

    def __str__(self):
        return 'Proyectista: ' + self.nombre



class Localidad(models.Model):
    slug = models.SlugField(primary_key=True, max_length=300, blank=True)
    nombre = models.CharField(max_length=300, unique=True)

    def __str__(self):
        return 'Localidad: ' + self.nombre

    class Meta:
        verbose_name_plural = "Localidades"

def año_pre_save_receiver(sender, instance, *args, **kwargs):
    """
    """
    instance.slug = slugify(instance.año)

def proyectista_pre_save_receiver(sender, instance, *args, **kwargs):
    """
    """
    instance.slug = slugify(instance.nombre)

def localidad_pre_save_receiver(sender, instance, *args, **kwargs):
    """
    """
    instance.slug = slugify(instance.nombre)


pre_save.connect(año_pre_save_receiver, sender=Año)
pre_save.connect(proyectista_pre_save_receiver, sender=Proyectista)
pre_save.connect(localidad_pre_save_receiver, sender=Localidad)
