import django.core.checks

from rest_framework import serializers
from .models import Servicio, Año, Proyectista, Localidad
from proyectos.models import Proyecto


@django.core.checks.register('rest_framework.serializers')
class ProyectoAuxThumbnailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Proyecto
        fields = ['slug', 'titulo', 'imagen_thumbnail', 'alt_imagen_thumbnail']


@django.core.checks.register('rest_framework.serializers')
class ServicioShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Servicio
        fields = ['nombre', 'get_nombre_display', 'mini_descripcion_home']

@django.core.checks.register('rest_framework.serializers')
class AñoShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Año
        fields = ['slug', 'año']

@django.core.checks.register('rest_framework.serializers')
class ProyectistaShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Proyectista
        fields = ['slug', 'nombre', 'pagina_web', 'logo', 'especialidad']

@django.core.checks.register('rest_framework.serializers')
class LocalidadShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Localidad
        fields = ['slug', 'nombre']


@django.core.checks.register('rest_framework.serializers')
class ServicioSerializer(serializers.ModelSerializer):
    servicio_proyecto = ProyectoAuxThumbnailSerializer(many=True)
    class Meta:
        model = Servicio
        fields = ['nombre', 'get_nombre_display', 'descripcion', 'descripcion_corta', 'meta_titulo', 'meta_descripcion', 'imagen', 'alt_imagen', 'servicio_proyecto', 'texto_contacto']