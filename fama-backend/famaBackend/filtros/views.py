from rest_framework.response import Response
from django.http import HttpResponse
from rest_framework.decorators import api_view
from .models import Servicio, Año, Proyectista, Localidad

from rest_framework import viewsets
from proyectos.models import Proyecto
from proyectos.serializers import ProyectoAñoThumbnailSerializer, ProyectoLocalidadThumbnailSerializer, ProyectoProyectistaThumbnailSerializer
from .serializers import ServicioShortSerializer, ServicioSerializer
from itertools import groupby

@api_view(['GET'])
def años_view(request):
    """
    """
    try:
        queryset = Proyecto.objects.all().order_by('-año__año')
        serializer = ProyectoAñoThumbnailSerializer(queryset, many=True)
        return Response(dict([(año, list(proyectos)) for año, proyectos in groupby(serializer.data, key=lambda x: x['año']['slug'])]))
    except:
        return HttpResponse(status=400)

@api_view(['GET'])
def localidad_view(request):
    """
    """
    try:
        queryset = Proyecto.objects.all().order_by('-año__año')
        serializer = ProyectoLocalidadThumbnailSerializer(queryset, many=True)
        return Response( dict([(localidad, list(proyectos)) for localidad, proyectos in groupby(serializer.data, key=lambda x: x['localidad']['nombre'])]))
    except:
        return HttpResponse(status=400)

@api_view(['GET'])
def proyectista_view(request):
    """
    """
    try:
        queryset = Proyecto.objects.all().order_by('-año__año')
        serializer = ProyectoProyectistaThumbnailSerializer(queryset, many=True)
        return Response( dict([(proyectista['nombre'], list(proyectos)) for proyectista, proyectos in groupby(serializer.data, key=lambda x: x['proyectista']) if proyectista]))
    except:
        return HttpResponse(status=400)

class ServiciosViewSet(viewsets.ModelViewSet):
    """
    """
    serializer_class = ServicioSerializer
    lookup_field = 'nombre'
    http_method_names = ['get']

    def get_queryset(self):
        return Servicio.objects.all()
  
    def list(self, request):
        queryset = self.get_queryset()
        serializer = ServicioShortSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, nombre):
        queryset = Servicio.objects.filter(nombre=nombre).first()
        serializer = ServicioSerializer(queryset, many=False)
        return Response(serializer.data)