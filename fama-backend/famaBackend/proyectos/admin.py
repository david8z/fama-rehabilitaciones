from django.contrib import admin
from django.contrib.auth.models import Group, User
# from rest_framework.authtoken.models import Token
from django import forms

admin.site.unregister(Group)
admin.site.unregister(User)
# admin.site.unregister(Token)
from django.urls import resolve
from .models import Proyecto, ImagenesProyecto, ProyectosRelacionados
from django.utils.html import mark_safe
from django.conf import settings
from grappelli.forms import GrappelliSortableHiddenMixin

from import_export import resources
from import_export.admin import ImportExportModelAdmin


class ProyectoResource(resources.ModelResource):
    class Meta:
        model = Proyecto
        import_id_fields = ['slug']

class ImagenesProyectoResource(resources.ModelResource):
    class Meta:
        model = ImagenesProyecto

class ProyectosRelacionadosInline(GrappelliSortableHiddenMixin, admin.TabularInline):
    model = ProyectosRelacionados
    fields = ("proyecto_imagen_display", "proyecto_relacionado", "position")
    readonly_fields = ("proyecto_imagen_display",)
    sortable_field_name = "position"
    extra = 0
    fk_name = "proyecto"
    verbose_name = "PROYECTOS RELACIONADOS"
    verbose_name_plural = "PROYECTOS RELACIONADOS"
    classes = ('grp-collapse grp-open',)

    def formfield_for_foreignkey(self, db_field, request=None, *args, **kwargs):
        if db_field.name == "proyecto_relacionado" and resolve(request.path_info).kwargs:
            kwargs["queryset"] = Proyecto.objects.exclude(slug=resolve(request.path_info).kwargs['object_id'])
        return super(ProyectosRelacionadosInline, self).formfield_for_foreignkey(db_field, request, **kwargs)

class ImagenesProyectoInline(GrappelliSortableHiddenMixin, admin.TabularInline):
    model = ImagenesProyecto
    fields = ("imagen1_display", "imagen1", "alt_imagen1", "imagen2_display", "imagen2", "alt_imagen2", "position")
    readonly_fields = ("imagen1_display", "imagen2_display")
    sortable_field_name = "position"
    extra = 0
    verbose_name = "IMÁGENES DE PROYECTO"
    verbose_name_plural = "IMÁGENES DE PROYECTO"
    classes = ('grp-collapse grp-open',)

class ImagenesProyectoModelAdmin(ImportExportModelAdmin):
    resource_class = ImagenesProyectoResource
    list_filter = ['proyecto', ]

    def get_model_perms(self, request):
        if request.user.username == 'admin':
            return {'add': True, 'change':True, 'delete':True}
        return {}

class ProyectoModelAdmin(ImportExportModelAdmin):
    resource_class = ProyectoResource
    list_display = ['titulo', 'año', 'proyectista', 'localidad', 'imagen', 'visualicaciones']
    list_filter = ['año', 'proyectista', 'localidad']

    def imagen(self, obj):
        if settings.DEBUG:
            return mark_safe(
                    '<img src="http://127.0.0.1:8080/media/%s" width="200" height="200">' % obj.imagen_thumbnail
                )
        else:
            return mark_safe(
                    '<img src="%s/media/%s" width="200" height="200">' % (settings.URL_PROD,obj.imagen_thumbnail)
                )

    def visualicaciones(self, obj):
        return obj.hit_count.hits


    def año(self, obj):
        return obj.año.slug

    def proyectista(self, obj):
        return obj.proyectista.nombre

    def localidad(self, obj):
        return obj.localidad.nombre

    fieldsets = (
        ('METADATOS', {
            'fields': (('meta_titulo', 'meta_descripcion'),),
            'classes': ('grp-collapse grp-closed',),
        }),
        ('CONTENIDOS', {
            'fields': ('titulo', 'descripcion', 'texto_contacto'),
            'classes': ('grp-collapse grp-open',),
        }),
        ('FILTROS', {
            'fields': (('servicio', 'año'), ('proyectista', 'localidad'),),
            'classes': ('grp-collapse grp-open',),
        }),
        ('IMÁGENES', {
            'fields': (('imagen_thumbnail', 'alt_imagen_thumbnail'), ('imagen_principal', 'alt_imagen_principal')),
            'classes': ('grp-collapse grp-open',),
        }),
    )

    inlines = [ImagenesProyectoInline, ProyectosRelacionadosInline, ]
    class Media:
        css = {
            'all': ('admin/css/custom-row.css',)
        }


admin.site.register(Proyecto, ProyectoModelAdmin)
admin.site.register(ImagenesProyecto, ImagenesProyectoModelAdmin)
