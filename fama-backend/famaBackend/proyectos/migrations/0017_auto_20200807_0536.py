# Generated by Django 3.1 on 2020-08-07 03:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('proyectos', '0016_auto_20200806_2307'),
    ]

    operations = [
        migrations.AlterField(
            model_name='proyectosrelacionados',
            name='proyecto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='proyectos_relacionados', to='proyectos.proyecto'),
        ),
        migrations.AlterField(
            model_name='proyectosrelacionados',
            name='proyecto_relacionado',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='assignee', to='proyectos.proyecto', verbose_name='Proyecto Relacionado*'),
        ),
    ]
