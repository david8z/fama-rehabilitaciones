from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from ckeditor.fields import RichTextField
from django.db.models.signals import pre_save
from django.utils.text import slugify
from filtros.models import Año, Localidad, Proyectista, Servicio
from django.conf import settings
from django.utils.html import mark_safe
from django.conf import settings



class Proyecto(models.Model):
    """
    """
    slug = models.SlugField(primary_key=True, max_length=100, blank=True)
    titulo = models.CharField(max_length=100, verbose_name="Título*", unique=True)
    descripcion = RichTextField(help_text="Descripción del proyecto", verbose_name="Descripción", null=True, blank=True)

    texto_contacto = models.CharField(max_length=100, null=True, blank=True, verbose_name="Texto de Contacto")

    meta_titulo = models.CharField(max_length=60, null=True, blank=True, help_text="Máximo 60 carácteres", verbose_name="Meta Título")
    meta_descripcion = models.TextField(max_length=120, null=True, blank=True, help_text="Máximo 120 carácteres", verbose_name="Meta Descripción")

    imagen_thumbnail = models.ImageField(upload_to='proyectos', help_text="Será la primera imágen que aparecerá en los recuadros de selección del proyecto formato recomendado 1:1.", verbose_name="Imagen Thumbnail*", null=True)
    alt_imagen_thumbnail = models.TextField(max_length=300, null=True, blank=True, verbose_name='Alt')

    imagen_principal = models.ImageField(upload_to='proyectos', help_text="Será la primera imágen que aparecerá en la página de proyecto.", verbose_name="Imagen Principal*", null=True)
    alt_imagen_principal = models.TextField(max_length=300, null=True, blank=True, verbose_name='Alt')

    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE, related_name="servicio_proyecto", db_index=True, verbose_name="Servicio*", null=True)
    año = models.ForeignKey(Año, on_delete=models.SET_NULL, related_name="año_proyecto", null=True, verbose_name="Año*")
    proyectista = models.ForeignKey(Proyectista, on_delete=models.SET_NULL, related_name="poyectista_proyecto", null=True, blank=True, verbose_name="Proyectista")
    localidad = models.ForeignKey(Localidad, on_delete=models.SET_NULL, related_name="localidad_proyecto", null=True, verbose_name="Localidad*")


    def __str__(self):
        return 'Proyecto: ' + self.titulo

    def get_image_path(self):
        if settings.DEBUG:
            return 'http://127.0.0.1:8080%s' % self.imagen_thumbnail.url
        else:
            return '%s%s' % (settings.URL_PROD, self.imagen_thumbnail.url)

    class Meta:
        ordering = ['-año__año']


class ImagenesProyecto(models.Model):
    """
    """
    imagen1 = models.ImageField(upload_to='proyectos', verbose_name="Imagen 1*", null=True)
    alt_imagen1 = models.TextField(max_length=300, null=True, blank=True, verbose_name='Alt')

    imagen2 = models.ImageField(upload_to='proyectos', help_text="Es opcional en caso de haber cuando este en pantalla de ordenador aparacerá la imagen1 y la imagen2 una junto a la otra.", verbose_name="Imagen 2", null=True, blank=True)
    alt_imagen2 = models.TextField(max_length=300, null=True, blank=True, verbose_name='Alt')

    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE, related_name="imagenes_proyecto")
    position = models.PositiveSmallIntegerField(verbose_name="Posición", null=True, blank=True, help_text="Dicatará el orden en el que aparecen las imágenes. Se genera de forma automática.",)
    def imagen1_display(self):
        if not self.imagen1:
            return "SIN IMÁGEN"

        if settings.DEBUG:
            return mark_safe(
                    '<img src="http://127.0.0.1:8080/media/%s" width="200" height="200">' % self.imagen1
                )
        else:
            return mark_safe(
                    '<img src="%s/media/%s" width="200" height="200">' % (settings.URL_PROD, self.imagen1)
                )
    def imagen2_display(self):
        if not self.imagen2:
            return "SIN IMÁGEN"

        if settings.DEBUG:
            return mark_safe(
                    '<img src="http://127.0.0.1:8080/media/%s" width="200" height="200">' % self.imagen2
                )
        else:
            return mark_safe(
                    '<img src="%s/media/%s" width="200" height="200">' % (settings.URL_PROD, self.imagen2)
                )
    imagen1_display.short_description = 'Imágen 1 Display'
    imagen1_display.allow_tags = True
    imagen2_display.short_description = 'Imágen 2 Display'
    imagen2_display.allow_tags = True
    class Meta:
        ordering = ['position']

class ProyectosRelacionados(models.Model):
    """
    """
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE, related_name='proyectos_relacionados')
    proyecto_relacionado = models.ForeignKey(Proyecto, on_delete=models.CASCADE, related_name='assignee', verbose_name="Proyecto Relacionado*")
    position = models.PositiveSmallIntegerField(verbose_name="Posición", null=True, blank=True, help_text="Dicatará el orden en el que aparecen las imágenes. Se genera de forma automática.",)

    def proyecto_imagen_display(self):
        if not self.proyecto_relacionado:
            return "SIN IMÁGEN"

        if settings.DEBUG:
            return mark_safe(
                    '<img src="http://127.0.0.1:8080/media/%s" width="200" height="200">' % self.proyecto_relacionado.imagen_thumbnail
                )
        else:
            return mark_safe(
                    '<img src="%s/media/%s" width="200" height="200">' % (settings.URL_PROD, self.proyecto_relacionado.imagen_thumbnail)
                )
    proyecto_imagen_display.short_description = 'Imágen Proyecto Display'
    proyecto_imagen_display.allow_tags = True


def proyecto_pre_save_receiver(sender, instance, *args, **kwargs):
    """
    """
    instance.slug = slugify(instance.titulo)


pre_save.connect(proyecto_pre_save_receiver, sender=Proyecto)