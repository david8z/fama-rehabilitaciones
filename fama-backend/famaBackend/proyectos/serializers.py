import django.core.checks

from rest_framework import serializers
from .models import Proyecto, ProyectosRelacionados, ImagenesProyecto
from filtros.serializers import ProyectistaShortSerializer, AñoShortSerializer, ServicioShortSerializer, LocalidadShortSerializer

@django.core.checks.register('rest_framework.serializers')
class ProyectoThumbnailSerializer(serializers.ModelSerializer):
    servicio = ServicioShortSerializer(many=False)
    class Meta:
        model = Proyecto
        fields = ['slug', 'titulo', 'imagen_thumbnail', 'alt_imagen_thumbnail', 'servicio']

@django.core.checks.register('rest_framework.serializers')
class ProyectoAñoThumbnailSerializer(serializers.ModelSerializer):
    año = AñoShortSerializer(many=False)
    servicio = ServicioShortSerializer(many=False)
    class Meta:
        model = Proyecto
        fields = ['slug', 'titulo', 'imagen_thumbnail', 'alt_imagen_thumbnail', 'año', 'servicio']

@django.core.checks.register('rest_framework.serializers')
class ProyectoLocalidadThumbnailSerializer(serializers.ModelSerializer):
    localidad = LocalidadShortSerializer(many=False)
    servicio = ServicioShortSerializer(many=False)
    class Meta:
        model = Proyecto
        fields = ['slug', 'titulo', 'imagen_thumbnail', 'alt_imagen_thumbnail', 'localidad', 'servicio']

@django.core.checks.register('rest_framework.serializers')
class ProyectoProyectistaThumbnailSerializer(serializers.ModelSerializer):
    proyectista = ProyectistaShortSerializer(many=False)
    servicio = ServicioShortSerializer(many=False)
    class Meta:
        model = Proyecto
        fields = ['slug', 'titulo', 'imagen_thumbnail', 'alt_imagen_thumbnail', 'proyectista', 'servicio']

@django.core.checks.register('rest_framework.serializers')
class ProyectosRelacionadosSerializer(serializers.ModelSerializer):
    proyecto_relacionado = ProyectoThumbnailSerializer(many=False)
    class Meta:
        model = ProyectosRelacionados
        fields = ['proyecto_relacionado', ]


@django.core.checks.register('rest_framework.serializers')
class ProyectosRelacionadosSerializer(serializers.ModelSerializer):
    proyecto_relacionado = ProyectoThumbnailSerializer(many=False)
    class Meta:
        model = ProyectosRelacionados
        fields = ['proyecto_relacionado', ]

@django.core.checks.register('rest_framework.serializers')
class ImagenesProyectoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImagenesProyecto
        fields = ['imagen1', 'alt_imagen1', 'imagen2', 'alt_imagen2' ]

@django.core.checks.register('rest_framework.serializers')
class ProyectoSerializer(serializers.ModelSerializer):
    imagenes_proyecto = ImagenesProyectoSerializer(many=True)
    proyectos_relacionados = ProyectosRelacionadosSerializer(many=True)
    servicio = ServicioShortSerializer(many=False)
    año = AñoShortSerializer(many=False)
    proyectista = ProyectistaShortSerializer(many=False)
    localidad = LocalidadShortSerializer(many=False)
    class Meta:
        model = Proyecto
        fields = ['slug', 'titulo', 'descripcion', 'meta_titulo', 'meta_descripcion', 'imagen_thumbnail', 'alt_imagen_thumbnail', 'imagen_principal', 'alt_imagen_principal', 'servicio', 'año', 'proyectista', 'localidad', 'imagenes_proyecto', 'proyectos_relacionados', 'texto_contacto']
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }