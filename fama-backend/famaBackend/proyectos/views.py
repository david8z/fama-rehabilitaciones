from rest_framework import viewsets
from .models import Proyecto
from .serializers import ProyectoSerializer
from rest_framework.response import Response

class ProyectoViewSet(viewsets.ModelViewSet):
    """
    """
    serializer_class = ProyectoSerializer
    lookup_field = 'slug'
    http_method_names = ['get']

    def get_queryset(self):
        return Proyecto.objects.all().order_by('-año__año')
  
    def list(self, request):
        queryset = self.get_queryset()
        serializer = ProyectoSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, slug):
        queryset = Proyecto.objects.filter(slug=slug).first()
        serializer = ProyectoSerializer(queryset, many=False)
        return Response(serializer.data)