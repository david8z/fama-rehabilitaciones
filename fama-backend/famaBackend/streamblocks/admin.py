# streamblocks/admin.py

from django.contrib import admin
from streamfield.admin import StreamBlocksAdmin

from streamblocks.models import ProyectoBlock, EquipoBlock, MiembrosEquipo, ListaProyectosBlock, ProyectoListaProyectos,ImagenConMensajeBlock, EnlaceServiciosBlock
from streamfield.fields import StreamFieldWidget
from .models import Page

from django.utils.html import mark_safe
from django.conf import settings
from grappelli.forms import GrappelliSortableHiddenMixin


admin.site.unregister(ProyectoBlock)
admin.site.unregister(EquipoBlock)
admin.site.unregister(ListaProyectosBlock)
admin.site.unregister(ImagenConMensajeBlock)
admin.site.unregister(EnlaceServiciosBlock)

@admin.register(EnlaceServiciosBlock)
class EnlaceServiciosBlockAdmin(StreamBlocksAdmin, admin.ModelAdmin):
    fieldsets = (
        ('PRINCIPAL', {
            'fields': ('titulo', )
        }),
    )
    def get_model_perms(self, request):
        if request.user.username == 'admin':
            return {'add': True, 'change':True, 'delete':True}
        return {}

@admin.register(ImagenConMensajeBlock)
class ImagenConMensajeBlockAdmin(StreamBlocksAdmin, admin.ModelAdmin):
    fieldsets = (
        ('PRINCIPAL', {
            'fields': ('mensaje', ('imagen', 'alt_imagen'))
        }),
    )
    def get_model_perms(self, request):
        if request.user.username == 'admin':
            return {'add': True, 'change':True, 'delete':True}
        return {}

class ProyectoListaProyectosInline(GrappelliSortableHiddenMixin, admin.TabularInline):
    model = ProyectoListaProyectos
    fields = ("proyecto", "position")
    sortable_field_name = "position"
    extra = 0
    verbose_name = "Lista de Proyectos"
    verbose_name_plural = "Lista de Proyectos"

@admin.register(ListaProyectosBlock)
class ListaProyectosBlockAdmin(StreamBlocksAdmin, admin.ModelAdmin):
    fieldsets = (
        ('PRINCIPAL', {
            'fields': ('titulo',)
        }),
    )
    inlines = [ProyectoListaProyectosInline, ]
    def get_model_perms(self, request):
        if request.user.username == 'admin':
            return {'add': True, 'change':True, 'delete':True}
        return {}

class MiembrosEquipoInline(GrappelliSortableHiddenMixin, admin.TabularInline):
    model = MiembrosEquipo
    fields = ("nombre", "puesto", "imagen", "position")
    sortable_field_name = "position"
    extra = 0
    verbose_name = "Miembros"
    verbose_name_plural = "Miembros"

@admin.register(ProyectoBlock)
class ProyectoBlockAdmin(StreamBlocksAdmin, admin.ModelAdmin):
    fieldsets = (
        ('PRINCIPAL', {
            'fields': ('proyecto', 'descripcion')
        }),
        ('EXTRA', {
            'fields': (('imagen', 'alt_imagen'), 'titulo', 'texto_boton',),
            'classes': ('grp-collapse grp-closed',),
        }),
    )
    def get_model_perms(self, request):
        if request.user.username == 'admin':
            return {'add': True, 'change':True, 'delete':True}
        return {}

@admin.register(EquipoBlock)
class EquipoBlockAdmin(StreamBlocksAdmin, admin.ModelAdmin):
    fieldsets = (
        ('PRINCIPAL', {
            'fields': ('titulo', 'descripcion', 'portfolio')
        }),
    )
    inlines = [MiembrosEquipoInline, ]
    def get_model_perms(self, request):
        if request.user.username == 'admin':
            return {'add': True, 'change':True, 'delete':True}
        return {}


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    list_display = ('nombre',)
    fieldsets = (
        ('METADATOS', {
            'fields': (('meta_titulo', 'meta_descripcion'),),
            'classes': ('grp-collapse grp-closed',),
        }),
        ('PRINCIPAL', {
            'fields': ('titulo', 'imagen', 'texto_contacto'),
            'classes': ('grp-collapse grp-open',),
        }),
        ('BLOQUES', {
            'fields': ('stream',),
            'classes': ('grp-collapse grp-open',),
        }),
    )
    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if obj and obj.id == 1:
            form.base_fields['stream'].widget = StreamFieldWidget(attrs={
                'model_list': [ProyectoBlock, EquipoBlock, ListaProyectosBlock, ImagenConMensajeBlock, EnlaceServiciosBlock]
                })
        return form

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions
    
    def has_add_permission(self, request, obj=None):
        return request.user.username == 'admin'
    
    def has_delete_permission(self, request, obj=None):
        return request.user.username == 'admin'

    class Media:
        css = {
            'all': ('admin/css/custom-streamblocks.css',)
        }