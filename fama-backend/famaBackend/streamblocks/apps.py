from django.apps import AppConfig


class StreamblocksConfig(AppConfig):
    name = 'streamblocks'
    verbose_name = 'Editor de Páginas'
