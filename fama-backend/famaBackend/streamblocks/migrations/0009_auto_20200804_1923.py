# Generated by Django 3.0.9 on 2020-08-04 17:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('streamblocks', '0008_proyectoblock_alt_imagen'),
    ]

    operations = [
        migrations.AlterField(
            model_name='proyectoblock',
            name='imagen',
            field=models.ImageField(blank=True, help_text='Imágen que mostrará el Proyecto, en caso de no declararla se usará la imagén thumbnail del Proyecto. Formato recomendado 1:1.', null=True, upload_to='bloques/home', verbose_name='Imagen Proyecto'),
        ),
    ]
