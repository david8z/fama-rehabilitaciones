# Generated by Django 3.0.9 on 2020-08-04 19:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('streamblocks', '0017_auto_20200804_2146'),
    ]

    operations = [
        migrations.AlterField(
            model_name='miembrosequipo',
            name='equipo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='miembro_equipo', to='streamblocks.EquipoBlock'),
        ),
    ]
