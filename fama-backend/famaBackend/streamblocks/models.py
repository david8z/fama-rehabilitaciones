from django.db import models
from streamfield.fields import StreamField
from proyectos.models import Proyecto
from ckeditor.fields import RichTextField
from django.db.models.signals import pre_save
from django.conf import settings

# ENLACE A SERVICIOS
class EnlaceServiciosBlock(models.Model):

    titulo = models.CharField(max_length=200, verbose_name="Título*", default="SERVICIOS")
    
    def __str__(self):
        return "Bloque Enlaces: " + self.titulo
    class Meta:
        verbose_name = "Lista Enlaces a Servicios"

# IMAGEN CON MENSAJE
class ImagenConMensajeBlock(models.Model):
    imagen = models.ImageField(upload_to='bloques/images_con_mensaje', verbose_name="Imagen*")
    alt_imagen = models.TextField(max_length=300, null=True, blank=True, verbose_name='Alt')

    mensaje = models.CharField(max_length=300, verbose_name="Mensaje*")
    custom_admin_template = "streamblocks/admin/imagenConMensaje.html"
    
    def get_image_path(self):
        if settings.DEBUG:
            return 'http://127.0.0.1:8080%s' % self.imagen.url
        else:
            return '%s%s' % (settings.URL_PROD, self.imagen.url)
    
    def __str__(self):
        return "Imagen con Mensaje: " + self.mensaje
    class Meta:
        verbose_name = "Imagen con Mensaje"

# LISTA PROYECTOS

class ListaProyectosBlock(models.Model):
    titulo = models.CharField(verbose_name="Título*", default="ÚLTIMOS PROYECTOS", max_length=200)

    custom_admin_template = "streamblocks/admin/listaProyectosBlock.html"

    def __str__(self):
        return "Lista Proyectos: " + self.titulo

    class Meta:
        verbose_name = "Lista de Proyectos"

class ProyectoListaProyectos(models.Model):
    lista_proyectos = models.ForeignKey(ListaProyectosBlock, on_delete=models.CASCADE, related_name="lista_lista_proyectos")
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE, related_name="proyecto_lista_proyectos")
    
    position = models.PositiveSmallIntegerField(verbose_name="Posición", null=True, blank=True, help_text="Dicatará el orden en el que aparecen los miembros. Se genera de forma automática.",)
    class Meta:
        ordering = ['position']

# PROYECTO

class ProyectoBlock(models.Model):
    imagen = models.ImageField(upload_to='bloques/home', help_text="Imágen que mostrará el Proyecto, en caso de no declararla se usará la imagén thumbnail del Proyecto. Formato recomendado 1:1.", verbose_name="Imagen Proyecto", null=True, blank=True)
    alt_imagen = models.TextField(max_length=300, null=True, blank=True, verbose_name='Alt')

    custom_admin_template = "streamblocks/admin/proyectoBlock.html"

    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE, verbose_name="Proyecto*")

    titulo = models.CharField(max_length=400, null=True, blank=True, help_text="En caso de dejar en blanco se usará por defecto 'Proyecto en {localidad del proyecto}")
    descripcion = RichTextField(help_text="Descripción muy breve del proyecto", verbose_name="Descripción Breve*")

    texto_boton = models.CharField(max_length=100, default='Saber más', help_text="Texto del botón que te llevará al proyecto.", verbose_name="Texto Botón*")

    def get_image_path(self):
        if settings.DEBUG:
            return 'http://127.0.0.1:8080%s' % self.imagen.url
        else:
            return '%s%s' % (settings.URL_PROD, self.imagen.url)
    
    def __str__(self):
        return "Bloque Proyecto: " + self.proyecto.titulo

    class Meta:
        verbose_name = "Proyecto"

# EQUIPO

class EquipoBlock(models.Model):
    titulo = models.CharField(verbose_name="Título Diapositiva Equipo*", default="EQUIPO COMPROMETIDO", max_length=200)
    descripcion = RichTextField(help_text="Información del equipo", verbose_name="Descripción*", null=True)
    portfolio = models.FileField(upload_to="portfolio", verbose_name="Portfolio", null=True, blank=True)

    custom_admin_template = "streamblocks/admin/equipoBlock.html"

    def __str__(self):
        return "Bloque Equipo: " + self.titulo
    class Meta:
        verbose_name = "Equipo"

class MiembrosEquipo(models.Model):
    nombre = models.CharField(verbose_name="Nombre*", max_length=200, null=True)
    puesto = models.CharField(verbose_name="Puesto*", max_length=200, null=True)

    imagen = models.ImageField(upload_to='equipo', help_text="Formato recomendado 1:1.", verbose_name="Imagen*", null=True)
    alt_imagen = models.TextField(max_length=300, null=True, blank=True, verbose_name='Alt')
    equipo = models.ForeignKey(EquipoBlock, on_delete=models.CASCADE, related_name="miembro_equipo")
    position = models.PositiveSmallIntegerField(verbose_name="Posición", null=True, blank=True, help_text="Dicatará el orden en el que aparecen los miembros. Se genera de forma automática.",)

    def __str__(self):
        return "Miembro Equipo: " + self.nombre
    class Meta:
        ordering = ['position']


# Register blocks for StreamField as list of models
STREAMBLOCKS_MODELS = [
    ProyectoBlock,
    EquipoBlock,
    ListaProyectosBlock,
    ImagenConMensajeBlock,
    EnlaceServiciosBlock
]


class Page(models.Model):
    nombre = models.CharField(max_length=100, default="HOME", unique=True)

    titulo = models.CharField(max_length=100, default="CONSTRUIMOS HOGARES", verbose_name="Título*")
    imagen = models.ImageField(upload_to='home', verbose_name="Imagen*", null=True)

    texto_contacto = models.CharField(max_length=100, null=True, blank=True, verbose_name="Texto de Contacto")

    meta_titulo = models.CharField(max_length=60, null=True, blank=True, help_text="Máximo 60 carácteres", verbose_name="Meta Título")
    meta_descripcion = models.TextField(max_length=120, null=True, blank=True, help_text="Máximo 120 carácteres", verbose_name="Meta Descripción")
    stream = StreamField(
        model_list=[
            ProyectoBlock,
            EquipoBlock,
            ListaProyectosBlock,
            ImagenConMensajeBlock,
            EnlaceServiciosBlock
        ],
        verbose_name="BLOQUES HOME"
        )
    def __str__(self):
        return "Página: " + self.nombre
    
    class Meta:
        verbose_name = "Página"
        verbose_name_plural = "Páginas"



def proyecto_block_pre_save_receiver(sender, instance, *args, **kwargs):
    """
    """
    if not instance.imagen:
        instance.imagen = instance.proyecto.imagen_thumbnail
    if not instance.alt_imagen and instance.proyecto.alt_imagen_thumbnail and instance.imagen == instance.proyecto.imagen_thumbnail:
        instance.alt_imagen = instance.proyecto.alt_imagen_thumbnail
    if not instance.titulo:
        instance.titulo = "Proyecto en " + instance.proyecto.localidad.nombre


pre_save.connect(proyecto_block_pre_save_receiver, sender=ProyectoBlock)   
