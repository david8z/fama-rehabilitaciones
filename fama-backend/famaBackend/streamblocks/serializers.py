import django.core.checks


from rest_framework import serializers
from .models import Page, EnlaceServiciosBlock, ImagenConMensajeBlock, ListaProyectosBlock, ProyectoListaProyectos, ProyectoBlock, EquipoBlock, MiembrosEquipo
from proyectos.serializers import ProyectoThumbnailSerializer

@django.core.checks.register('rest_framework.serializers')
class EnlaceServiciosBlockSerializer(serializers.ModelSerializer):
    class Meta:
        model = EnlaceServiciosBlock
        fields = ['titulo',]

@django.core.checks.register('rest_framework.serializers')
class ImagenConMensajeBlockSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImagenConMensajeBlock
        fields = ['imagen', 'alt_imagen', 'mensaje']

@django.core.checks.register('rest_framework.serializers')
class ProyectoListaProyectosSerializer(serializers.ModelSerializer):
    proyecto = ProyectoThumbnailSerializer(many=False)
    class Meta:
        model = ProyectoListaProyectos
        fields = ['proyecto',]

@django.core.checks.register('rest_framework.serializers')
class ListaProyectosBlockSerializer(serializers.ModelSerializer):
    lista_lista_proyectos = ProyectoListaProyectosSerializer(many=True)
    class Meta:
        model = ListaProyectosBlock
        fields = ['titulo', 'lista_lista_proyectos', ]

@django.core.checks.register('rest_framework.serializers')
class ProyectoBlockSerializer(serializers.ModelSerializer):
    proyecto = ProyectoThumbnailSerializer(many=False)
    class Meta:
        model = ProyectoBlock
        fields = ['imagen', 'alt_imagen', 'titulo', 'descripcion', 'texto_boton', 'proyecto']

@django.core.checks.register('rest_framework.serializers')
class MiembrosEquipoSerializer(serializers.ModelSerializer):
    class Meta:
        model = MiembrosEquipo
        fields = ['nombre', 'puesto', 'imagen', 'alt_imagen']

@django.core.checks.register('rest_framework.serializers')
class EquipoBlockSerializer(serializers.ModelSerializer):
    miembro_equipo = MiembrosEquipoSerializer(many=True)
    class Meta:
        model = EquipoBlock
        fields = ['titulo', 'descripcion', 'portfolio', 'miembro_equipo']

########
##PAGE##
########
@django.core.checks.register('rest_framework.serializers')
class PageSerializer(serializers.ModelSerializer):
    """DOCSTRING"""
    elementos = serializers.SerializerMethodField('stream_convertor')

    def stream_convertor(self, obj):
        
        aux_list = []
        for elemento in obj.stream.as_list():
            aux = {}
            if elemento['data']['block_model'] == 'enlaceserviciosblock':
                aux = EnlaceServiciosBlockSerializer(elemento['data']['block_content'], many=False).data
                aux['elemento'] = elemento['data']['block_model']
            elif elemento['data']['block_model'] == 'proyectoblock':
                aux = ProyectoBlockSerializer(elemento['data']['block_content'], many=False).data
                aux['elemento'] = elemento['data']['block_model']
            elif elemento['data']['block_model'] == 'listaproyectosblock':
                aux = ListaProyectosBlockSerializer(elemento['data']['block_content'], many=False).data
                aux['elemento'] = elemento['data']['block_model']
            elif elemento['data']['block_model'] == 'equipoblock':
                aux = EquipoBlockSerializer(elemento['data']['block_content'], many=False).data
                aux['elemento'] = elemento['data']['block_model']
            elif elemento['data']['block_model'] == 'imagenconmensajeblock':
                aux = ImagenConMensajeBlockSerializer(elemento['data']['block_content'], many=False).data
                aux['elemento'] = elemento['data']['block_model']

            aux_list.append(aux)
        return aux_list

    class Meta:
        model = Page
        fields = ['titulo', 'imagen', 'meta_titulo', 'meta_descripcion', 'stream', 'elementos', 'texto_contacto']

