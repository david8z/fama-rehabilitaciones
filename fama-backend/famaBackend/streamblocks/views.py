from rest_framework.response import Response
from django.http import HttpResponse
from rest_framework.decorators import api_view
from .models import Page
from .serializers import PageSerializer

@api_view(['GET'])
def page_view(request):
    """
    API endpoint permite obtener información configuración requerida en server.
    """
    try:
        queryset = Page.objects.first()
        serializer = PageSerializer(queryset, many=False)
        return Response(serializer.data)
    except:
        return HttpResponse(status=400)

        
