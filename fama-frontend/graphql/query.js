import { gql } from 'nuxt-graphql-request'

export const paginaHomeQuery = gql`
query paginaHomeEntryQuery {
  paginaHome(id: "2jxsEpMYiy87ma7pg5pyYj") {
    metaTitulo,
    metaDescripcion,
    titulo,
    imagen,
    bloquesCollection {
      items {
        __typename,
        ... on  BloqueImagenConMensaje {
          mensaje,
          imagen
        }
        ... on BloqueListaProyectos {
          titulo,
          proyectosCollection (limit: 12) {
            items {
              ...ProyectoFragment
            }
          }
        }
        ... on BloqueListaServicios {
          titulo,
          serviciosCollection (limit: 6) {
            items {
              titulo,
              slug,
              descripcionHomePage
            }
          }
        }
        ... on BloqueMiembrosEquipo {
          titulo,
          descripcion {
            json
          },
          portfolio {
            url
          },
          miembrosEquipoCollection (limit: 12) {
            items{
              nombre,
              puesto,
              imagen
            }
          }
        }
        ... on BloqueProyecto {
          titulo,
          descripcion {
            json
          },,
          textoBoton,
          imagenAlternativa,
          proyecto {
            ...ProyectoFragment
          }
        }
      }
    }
  }
}

fragment ProyectoFragment on Proyecto {
  titulo,
  slug,
  imagenPrincipal,
  servicio {
    slug
  }
}
`

export const listaProyectosQuery = gql`
query proyectoCollectionQuery{
  proyectoCollection {
    items {
      slug,
      servicio {
        slug
      }
    }
  }
}
`

export const paginaProyectoQuery = gql`
query proyectoCollectionQuery ($slug: String!) {
  proyectoCollection(where: {slug: $slug}, limit: 1) {
    items {
      metaTitulo,
      metaDescripcion,
      titulo,
      descripcion {json},
      servicio {
        titulo,
        slug
      },
      proyectista { nombre, slug, especialidad },
      localidad { nombreLocalidad, slug},
      ano,
      imagenPrincipal,
      imagenesProyecto,
      proyectosRelacionadosCollection (limit: 12) {
        items  {
          titulo,
          slug,
          imagenPrincipal,
          servicio {slug }
        }
      }
    }
  }
}
`

export const datosConfigQuery = gql`
query datosBaseEntryQuery {
  datosBase(id: "1QQh4LMj1Mgv8WbLDu3t4K") {
    pixelFacebook,
    googleAnalytics,
    favicon { url }
  }
}
`

export const datosBaseQuery = gql`
query datosBaseEntryQuery {
  datosBase(id: "1QQh4LMj1Mgv8WbLDu3t4K") {
    nombreEmpresa,
    email,
    telefonoFijo,
    telefonoMovil,
    direccionLinea1,
    direccionLinea2,
    enlaceFacebook,
    enlaceInstagram,
    logoClaro,
    logoOscuro
  }
}
`

export const listaServiciosQuery = gql`
query servicioCollectionQuery {
  servicioCollection {
    items {
      slug
    }
  }
}
`
export const serviciosQuery = gql`
query servicioCollectionQuery($slug: String!) {
  servicioCollection (where: {slug: $slug}, limit: 1) {
    items {
      metaTitulo,
      metaDescripcion,
      titulo,
      slug,
      imagen,
      descripcionCorta,
      descripcion {
        json
      },
      linkedFrom {
        proyectoCollection {
          items {
            slug,
            titulo,
            imagenPrincipal,
            servicio { slug }
          }
        }
      }
    }
  }
}
`

export const localidadQuery = gql`
query localidadCollectionQuery {
  localidadCollection (limit: 20) {
    items {
      nombreLocalidad,
      slug,
      linkedFrom {
        proyectoCollection {
          items {
            titulo,
            slug,
            imagenPrincipal,
            servicio { slug }
          }
        }
      }
    }
  }
}
`

export const proyectistaQuery = gql`
query proyectistaCollectionQuery {
  proyectistaCollection (limit: 20) {
    items {
      especialidad,
      nombre,
      paginaWeb,
      logo,
      slug,
      linkedFrom {
        proyectoCollection {
          items {
            titulo,
            slug,
            servicio { slug },
            imagenPrincipal
          }
        }
      }
    }
  }
}
`

export const anoQuery = gql`
query proyectoCollectionQuery {
  proyectoCollection {
    items {
      titulo,
      slug,
      servicio { slug },
      ano,
      imagenPrincipal
    }
  }
}
`
