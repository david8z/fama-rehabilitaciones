import colors from 'vuetify/es5/util/colors'
import { GraphQLClient } from 'graphql-request'
import { datosConfigQuery, listaProyectosQuery, listaServiciosQuery } from './graphql/query'

export default async () => {
  const baseUrl = process.env.NODE_ENV !== 'production' ? 'http://127.0.0.1:3000' : 'https://famarehabilitaciones.com'

  const graphQLClient = new GraphQLClient(process.env.CONTENTFUL_ENDPOINT, {
    headers: {
      authorization: `Bearer ${process.env.CONTENTFUL_TOKEN}`
    }
  })
  const datosConfigServer = await graphQLClient.request(datosConfigQuery)

  return {

    target: 'static',

    messages: {
      loading: 'Cargando...',
      error_404: 'Esta página no existe',
      server_error: 'Error del servidor'
    },
    /*
  ** Headers of the page
  */
    head: {
      title: process.env.npm_package_name || '',
      meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
      ],
      link: [
        { rel: 'icon', type: 'image/x-icon', href: datosConfigServer.favicon ? datosConfigServer.favicon.url : '' },
        { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.0/animate.css' }
      ],
      script: [
        { src: 'https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js' }
      ]
    },
    /*
  ** Customize the progress-bar color
  */
    loading: { color: '#fff' },
    /*
  ** Global CSS
  */
    css: [
    ],
    /*
  ** Plugins to load before mounting the App
  */
    plugins: [
      '~/plugins/breakpoint.js',
      '~/plugins/jsonld.js',
      { src: '@/plugins/wow', ssr: false }
    ],
    /*
  ** Nuxt.js dev-modules
  */
    buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
      '@nuxtjs/eslint-module',
      '@nuxtjs/vuetify',
      '@nuxtjs/pwa',
      'nuxt-graphql-request'
    ],
    /*
  ** Nuxt.js modules
  */
    modules: [
      ['nuxt-gmaps', {
        key: process.env.GMAPS_TOKEN
      }],
      '@neneos/nuxt-animate.css',
      '@nuxtjs/google-analytics',
      // 'nuxt-facebook-pixel-module',
      '@nuxtjs/robots',
      '@nuxtjs/sitemap',
      '@nuxtjs/cloudinary'
    ],

    graphql: {
      clients: {
        default: {
          endpoint: process.env.CONTENTFUL_ENDPOINT,
          options: {
            headers: {
              authorization: `Bearer ${process.env.CONTENTFUL_TOKEN}`
            }
          }
        }
      }
    },

    cloudinary: {
      cloudName: process.env.CLOUDINARY_CLOUDNAME,
      apiKey: process.env.CLOUDINARY_APIKEY,
      apiSecret: process.env.CLOUDINARY_APISECRET,
      useComponent: true
    },

    robots: {
      UserAgent: '*',
      Sitemap: baseUrl + '/sitemap.xml'
    },
    sitemap: {
      hostname: baseUrl,
      path: 'sitemap.xml',
      sitemaps: [
        {
          path: '/sitemaps/proyectos.xml',
          gzip: false,
          exclude: [
            '/filtros',
            ''
          ],
          routes () {
            return graphQLClient.request(listaProyectosQuery).then((res) => {
              const aux = []
              const items = res.proyectoCollection.items
              for (let i = 0; i < items.length; i++) {
                aux.push('/' + items[i].servicio.slug + '/' + items[i].slug)
              }
              return aux
            }).catch((errorRes) => {
              return []
            })
          }
        },
        {
          path: '/sitemaps/servicios.xml',
          gzip: false,
          exclude: [
            '/filtros',
            ''
          ],
          routes () {
            return graphQLClient.request(listaServiciosQuery).then((res) => {
              const aux = []
              const items = res.servicioCollection.items
              for (let i = 0; i < items.length; i++) {
                aux.push('/' + items[i].slug)
              }
              return aux
            }).catch((errorRes) => {
              return []
            })
          }
        }
      ]
    },
    // TRACKING AND ANALYTICS
    // facebook: {
    //   track: 'PageView',
    //   pixelId: process.env.NODE_ENV !== 'production' ? 'test' : datosConfigServer.pixelFacebook,
    //   disabled: process.env.NODE_ENV !== 'production' || !datosConfigServer.pixelFacebook
    // },
    googleAnalytics: {
      id: datosConfigServer.googleAnalytics,
      dev: process.env.NODE_ENV === 'production'
    },
    pwa: {
      meta: {
        name: '🥇Fama Rehabilitaciones | Más de 20 años de experiencia',
        author: 'David Alarcón Segarra',
        description: 'Descubre nuestros proyectos de 【Rehabilitación, Obra Nueva e Interiosimo】más de 700 viviendas en la Comunidad Valenciana',
        lang: 'es',
        ogHost: 'https://famarehabilitaciones.com'
      }
    },
    /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
    vuetify: {
      treeShake: true,
      defaultAssets: {
        font: {
          family: 'Open Sans'
        }
      },
      theme: {
        dark: true,
        themes: {
          dark: {
            primary: colors.blue.darken2,
            accent: colors.grey.darken3,
            secondary: colors.amber.darken3,
            info: colors.teal.lighten1,
            warning: colors.amber.base,
            error: colors.deepOrange.accent4,
            success: colors.green.accent3
          }
        }
      }
    },
    /*
  ** Build configuration
  */
    build: {
    /*
    ** You can extend webpack config here
    */
      extend (config, ctx) {
      }
    },
    server: {
      port: 3000, // default: 3000
      host: process.env.NODE_ENV !== 'production' ? '127.0.0.1' : '0.0.0.0' // default: localhost
    },
    env: {
      padding_left_md: '142px',
      padding_left_sm: '115px',
      padding_right_sm: '90px'
    }
  }
}
