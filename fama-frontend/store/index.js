import { datosBaseQuery } from '~/graphql/query'

export const state = () => ({
  configuracion: {}
})
export const mutations = {
  configuracion (state, newValue) {
    state.configuracion = newValue
  }
}

export const actions = {
  async nuxtServerInit ({ commit }) {
    const configQuery = await this.$graphql.default.request(datosBaseQuery)

    commit('configuracion', configQuery.datosBase)
  }
}

export const getters = {
  configuracion: state => state.configuracion
}
