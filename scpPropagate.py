import os
rootdir = '/home/david8z/Projects/fama_web/imagenes'

for subdir, dirs, files in os.walk(rootdir):
    if any([pattern in subdir for pattern in ['interiorismo/2017', 'interiorismo/2018']]):
        for file in files:
            os.system("scp -r {0}/{1} fama_user@161.35.80.60:~/fama-rehabilitaciones/fama-backend/media/proyectos".format(subdir.replace('&', '\&'), file.replace('&', '\&')))
